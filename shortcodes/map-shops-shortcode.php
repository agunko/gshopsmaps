<?php
function map_with_shops_shortcode_func() {
	$map_div = '<div id="shops-map"></div>';
	return $map_div;
}

add_shortcode( 'shops_map', 'map_with_shops_shortcode_func' );