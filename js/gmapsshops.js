var script = document.createElement('script');
script.type = 'text/javascript';
script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyCKO-8N_te2saaCLsuv6yfyb2dIzbWbk2A&callback=initMap';
document.head.appendChild(script);
var shopsinfo = Object.values(shops);

function initMap() {

    function addShopToMap(shop) {
        var marker = new google.maps.Marker({
            position: {lat: parseFloat(shop.lt), lng: parseFloat(shop.lg)},
            map: shopMap
        });
        var InfoWindow = new google.maps.InfoWindow({
            content:  "<h3>" + shop.title + "</h3><p>" + shop.description + "</p>"
        });
        marker.addListener('click', function () {
            InfoWindow.open(shopMap, marker);
        });
        shopMap.addListener('click', function () {
            InfoWindow.close(shopMap, marker);
        })
    }

    var element = document.getElementById('shops-map');
    element.setAttribute("style", "height:600px;");
    var options = {
        center: {lat: 37.4, lng: -120.4}
    };
    var shopMap = new google.maps.Map(element, options);
    shopMap.setZoom(4);
    for (let shopdata of shopsinfo) {
        addShopToMap(shopdata);
    }

}
