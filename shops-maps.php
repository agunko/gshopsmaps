<?php
/**
 * Plugin Name: Google Maps Shops Marker
 * Description: CPT Shops with shortcode [shops_map]
 * Text Domain: gmapsshops
 */


function gmaps_enqueue() {
	wp_register_script( 'gmapsshops', plugin_dir_url( __FILE__ ) . 'js/gmapsshops.js' );
}

add_action( 'wp_enqueue_scripts', 'gmaps_enqueue' );

function create_shop_post_type() {
	// Labels for post type
	$labels = array(
		'name'               => _x( 'Shops', 'post type general name', 'gmapsshops' ),
		'singular_name'      => _x( 'Shop', 'post type singular name', 'gmapsshops' ),
		'add_new'            => _x( 'Add New', 'Shop', 'gmapsshops' ),
		'add_new_item'       => __( 'Add New Shop' ),
		'edit_item'          => __( 'Edit Shop' ),
		'new_item'           => __( 'New Shop' ),
		'all_items'          => __( 'All Shops' ),
		'view_item'          => __( 'View Shop' ),
		'search_items'       => __( 'Search shops' ),
		'not_found'          => __( 'No shops found' ),
		'not_found_in_trash' => __( 'No shops found in the Trash' ),
		'parent_item_colon'  => '',
		'menu_name'          => __( 'Shops' ),
	);
	// Arguments for post type custom_shop
	$args = array(
		'labels'        => $labels,
		'description'   => __( 'Displays shops with maps' ),
		'public'        => true,
		'menu_position' => 3,
		'supports'      => array( 'title', 'custom-fields', ),
		'has_archive'   => true,
	);
	register_post_type( 'custom_shop', $args );
}

add_action( 'init', 'create_shop_post_type' );

function add_shops_fields_meta_box() {
	add_meta_box(
		'shop_info',
		'Shop Info',
		'show_shop_info_fields_meta_box',
		'custom_shop',
		'normal',
		'high'
	);
}

add_action( 'add_meta_boxes', 'add_shops_fields_meta_box' );

function show_shop_info_fields_meta_box() {
	//  Create metabox.
	wp_nonce_field( plugin_dir_path( __FILE__ ), 'shop_metabox' );
	// Including html metabox template file.
	include plugin_dir_path( __FILE__ ) . 'templates/shop-meta-box.php';
}

function save_shop_info_meta( $post_id ) {
	// Verify nonce
	if ( isset( $_POST['shop_metabox'] )
	     && ! wp_verify_nonce( $_POST['shop_metabox'], plugin_dir_path( __FILE__ ) ) ) {
		return $post_id;
	}
	// Check autosave
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return $post_id;
	}
	// Check permissions
	if ( isset( $_POST['post_type'] ) ) {
		if ( 'page' === $_POST['post_type'] ) {
			if ( ! current_user_can( 'edit_page', $post_id ) ) {
				return $post_id;
			} elseif ( ! current_user_can( 'edit_post', $post_id ) ) {
				return $post_id;
			}
		}
	}

	$old = get_post_meta( $post_id, 'shop_info', true );

	if ( isset( $_POST['shop_info'] ) ) {
		$new = $_POST['shop_info'];
		if ( $new && $new !== $old ) {
			$address   = $new['address'];
			$address   = urlencode( $address );
			$url       = "https://maps.googleapis.com/maps/api/geocode/json?address={$address}&key=AIzaSyCKO-8N_te2saaCLsuv6yfyb2dIzbWbk2A";
			$resp_json = file_get_contents( $url );
			$resp      = json_decode( $resp_json, true );
			if ( $resp['status'] == 'OK' ) {
				$lt              = $resp['results'][0]['geometry']['location']['lat'];
                $lg             = $resp['results'][0]['geometry']['location']['lng'];
				$formatted_address = $resp['results'][0]['formatted_address'];
			} else {
				$lt           = $resp['error_message'];
				$lg            = '';
				$formatted_address = '';
			}

			update_post_meta( $post_id, 'shop_info', $new );
			update_post_meta( $post_id, 'title', $new['title'] );
			update_post_meta( $post_id, 'address', $formatted_address );
			update_post_meta( $post_id, 'description', $new['description'] );
			update_post_meta( $post_id, 'latitude', $lt );
			update_post_meta( $post_id, 'longitude', $lg );
		} elseif ( '' === $new && $old ) {
			delete_post_meta( $post_id, 'shop_info', $old );
		}
	}
}

add_action( 'save_post', 'save_shop_info_meta' );

include plugin_dir_path( __FILE__ ) . 'shortcodes/map-shops-shortcode.php';

function custom_shortcode_scripts() {
	global $post;
	if ( has_shortcode( $post->post_content, 'shops_map' ) ) {
		$shops      = get_posts( array(
			'post_type'      => 'custom_shop',
			'posts_per_page' => -1

		) );
		$shops_data = array();
		foreach ( $shops as $shop ) {
			$shop_id = $shop->ID;
			$shops_data[ $shop_id ]['description'] = get_post_meta( $shop_id, 'description' );
			$shops_data[ $shop_id ]['lt'] = get_post_meta( $shop_id, 'latitude' );
			$shops_data[ $shop_id ]['lg'] = get_post_meta( $shop_id, 'longitude' );
			$shops_data[ $shop_id ]['title'] = get_post_meta( $shop_id, 'title' );
		}

		wp_localize_script(
			'gmapsshops',
			'shops',
			$shops_data
		);
		wp_enqueue_script( 'gmapsshops' );

	}
}

add_action( 'wp_enqueue_scripts', 'custom_shortcode_scripts' );