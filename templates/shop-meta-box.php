<?php
wp_enqueue_script( 'gmaps' );
global $post;
$metadata = get_post_meta( $post->ID, 'shop_info', true );
?>
<p>
    <label for="shop_info[name]"> <?php _e( 'Shop title' ); ?> </label>
    <br>
    <input type="text" name="shop_info[title]" id="shop_info[title]" class="regular-text"
           value="<?php if ( is_array( $metadata ) && isset( $metadata['title'] ) ) {
		       echo $metadata['title'];
	       } ?>">
</p>
<p>
    <label for="shop_info[description]"> <?php _e( 'Description' ); ?> </label>
    <br>
    <input type="text" name="shop_info[description]" id="shop_info[description]" class="regular-text"
           value="<?php if ( is_array( $metadata ) && isset( $metadata['description'] ) ) {
		       echo $metadata['description'];
	       } ?>">
</p>
<p>
    <label for="shop_info[address]"> <?php _e( 'Address' ) ?> </label>
    <br>
    <input type="text" id="shop_info[address]" name="shop_info[address]"
           value="<?php if ( is_array( $metadata ) && isset( $metadata['address'] ) ) {
		       echo $metadata['address'];
	       } ?>">
</p>